# Weather Station

This repo is used to store the source code for the Air Quality Weather station project by MobiusHorizons on youtube. 

## Dependencies

This code uses the following libraries:
- [OLED library](https://github.com/osresearch/esp32-ttgo/blob/master/libraries/LoRa.zip)
- [LoRa library](https://github.com/sandeepmistry/arduino-LoRa)

