// OLED pins to ESP32 GPIOs via this connecthin:
#define OLED_ADDRESS 0x3c
#define OLED_SDA 4 // GPIO4
#define OLED_SCL 15 // GPIO15
#define OLED_RST 16 // GPIO16

#include <Wire.h>
#include <SSD1306.h>

SSD1306 display(OLED_ADDRESS, OLED_SDA, OLED_SCL);

void display_reset() {
  digitalWrite(OLED_RST, LOW); // low to reset OLED
  delay(50); 
  digitalWrite(OLED_RST, HIGH); // must be high to turn on OLED
}

SSD1306 * display_init() {
	pinMode(OLED_RST,OUTPUT);
	display_reset();
	display.init();
	display.flipScreenVertically();

	return &display;
}
