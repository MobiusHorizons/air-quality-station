#include "packet.h"
#include "display.h"
#include "radio.h"

static SSD1306 * display;
static packet_t packet;

static void title() {
	display->setTextAlignment(TEXT_ALIGN_CENTER);
	display->setFont(ArialMT_Plain_16);
	display->drawString(64, 0, "Sender");
}

static void print_packet(packet_t p) { 
	display->setTextAlignment(TEXT_ALIGN_CENTER);
	display->setFont(ArialMT_Plain_24);

	display->drawString(64, 32, String("Packet: ") + String(p.sequence));
}

static void init_radio() {
	display->setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
	display->setFont(ArialMT_Plain_16);
	display->drawString(64, 32, "init...");
	display->display();

	radio_init();
}

static int send(packet_t * p) {
	int written = 0;

	if (LoRa.beginPacket() != 1) {
		return -1;
	}

	written = LoRa.write((const uint8_t *) p, packet_size);

	if (LoRa.endPacket() != 1) {
		return -2;
	}

	return written;
}

void sender_init() {
	display = display_init();
	init_radio();

	display->clear();
	title();
	display->display();
}

void sender_loop () {
	delay(5000);
	display->clear();

	send(&packet);

	title();
	print_packet(packet);

	display->display();
	packet.sequence++;
}
