#include "packet.h"
#include "display.h"
#include "radio.h"

static SSD1306 * display;
static packet_t packet;
static int have_new_packet = 0;

static void title() {
	display->setTextAlignment(TEXT_ALIGN_CENTER);
	display->setFont(ArialMT_Plain_16);
	display->drawString(64, 0, "Reciever");
}

static void print_rssi(int rssi) { 
	display->setTextAlignment(TEXT_ALIGN_CENTER);
	display->setFont(ArialMT_Plain_10);

	display->drawString(64, 22, String("RSSI: ") + String(rssi));
}

static void print_packet(packet_t p) { 
	display->setTextAlignment(TEXT_ALIGN_CENTER);
	display->setFont(ArialMT_Plain_10);

	display->drawString(64, 34, String("Packet: ") + String(p.sequence));
}

void on_receive(int size) {
	if (size == packet_size) {
		LoRa.readBytes((char *) &packet, size);
		have_new_packet = 1;
	}
}

static void init_radio() {
	display->setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
	display->setFont(ArialMT_Plain_16);
	display->drawString(64, 32, "init...");
	display->display();

	radio_init();
	LoRa.onReceive(on_receive);
	LoRa.receive();
}

void receiver_init() {
	display = display_init();
	init_radio();

	display->clear();
	title();
	display->display();
}

void receiver_loop () {
	if (have_new_packet == 1) {
		have_new_packet = 0;

		int rssi = LoRa.packetRssi();

		// display frame
		display->clear();

		title();
		print_rssi(rssi);
		print_packet(packet);

		display->display();
	}
}
