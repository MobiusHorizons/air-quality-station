#ifndef _PACKET_H_
#define _PACKET_H_

#include <stdint.h>

/**
 * This struct will be used to hold the packet data.
 * It is stored in a shared place so that we can use it both on the reciever
 * as well as on the sender side.
 * 
 * The struct is marked as packed so that no additional padding bytes are added.
 */
typedef struct __attribute__((__packed__)) {
	uint16_t sequence;
	// more fields to be added in the future
} packet_t;

#define packet_size ( sizeof(packet_t) )

#endif // _PACKET_H_
