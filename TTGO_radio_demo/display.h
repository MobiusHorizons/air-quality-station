#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <SSD1306.h>

void display_reset();
SSD1306 * display_init();

#endif // _DISPLAY_H_
